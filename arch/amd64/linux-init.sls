;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019, 2020, 2021 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; Linux-specific initialization

;; At this point the standard library has been loaded and should be
;; available.

;; This code runs in pid 0. Fibers are not available here.

(library (loko arch amd64 linux-init)
  (export)
  (import
    (rnrs (6))
    (only (loko runtime init) init-set!)
    (loko system unsafe)
    (loko runtime buddy)
    (loko runtime elf)
    (loko arch amd64 linux-numbers)
    (loko arch amd64 linux-syscalls)
    (loko arch amd64 processes)
    (only (loko runtime io) $init-standard-ports)
    (only (loko runtime scheduler) scheduler-loop)
    (except (loko system $host) $process-start)
    (loko system $primitives)
    (only (loko runtime context)
          CPU-VECTOR:ALTSIGSTK-BASE
          CPU-VECTOR:ALTSIGSTK-SIZE
          CPU-VECTOR:SCHEDULER-SP)
    (srfi :98 os-environment-variables))

;; Less general than the libc counterpart
(define (timer-create clock-id signal)
  (let ((evp (make-bytevector sizeof-sigevent))
        (timer-id (make-bytevector 8 0)))
    (bytevector-u32-native-set! evp offsetof-sigevent-sigev_signo signal)
    (bytevector-u32-native-set! evp offsetof-sigevent-sigev_notify SIGEV_SIGNAL)
    (let nuts ()
      (unless (sys_timer_create clock-id
                                (bytevector-address evp)
                                (bytevector-address timer-id)
                                (lambda (errno)
                                  (if (or (eqv? errno EAGAIN)
                                          (eqv? errno EINTR))
                                      #f
                                      (raise
                                        (make-syscall-error 'timer_create errno)))))
        (nuts)))
    (bytevector-u64-native-ref timer-id 0)))

;; Less general than the libc counterpart
(define (timer-settime timer seconds nanoseconds)
  (let ((itimerspec (make-bytevector sizeof-itimerspec))
        (flags 0)
        (NULL 0))
    ;; FIXME: better code for the offsets
    (bytevector-u64-native-set! itimerspec #x00 seconds)
    (bytevector-u64-native-set! itimerspec #x08 nanoseconds)
    (bytevector-u64-native-set! itimerspec #x10 seconds)
    (bytevector-u64-native-set! itimerspec #x18 nanoseconds)
    (sys_timer_settime timer flags (bytevector-address itimerspec)
                       NULL)))

(define (linux-init-signal-handlers)
  (define NULL 0)
  (define put! bytevector-u64-native-set!)
  (define (sigaction signal sa_sigaction sa_mask sa_flags)
    (let ((buf (make-bytevector sizeof-sigaction)))
      (bytevector-u64-native-set! buf offsetof-sigaction-sa_handler sa_sigaction)
      (bytevector-uint-set! buf offsetof-sigaction-sa_mask sa_mask (native-endianness)
                            sizeof-sigset_t)
      (bytevector-s32-native-set! buf offsetof-sigaction-sa_flags (fxior SA_RESTORER sa_flags))
      (bytevector-s64-native-set! buf offsetof-sigaction-sa_restorer
                                  ($linker-address 'linux:signal-return))
      (sys_rt_sigaction signal (bytevector-address buf) NULL sizeof-sigset_t)))
  (define (sigprocmask how set)
    (let ((buf (make-bytevector sizeof-sigset_t)))
      (bytevector-u64-native-set! buf 0 set)
      (sys_rt_sigprocmask how (bytevector-address buf) NULL (bytevector-length buf))))
  (define (sigaltstack ss_sp ss_size ss_flags)
    (let ((buf (make-bytevector sizeof-sigaltstack)))
      (bytevector-s64-native-set! buf offsetof-sigaltstack-ss_sp ss_sp)
      (bytevector-u64-native-set! buf offsetof-sigaltstack-ss_size ss_size)
      (bytevector-s32-native-set! buf offsetof-sigaltstack-ss_flags ss_flags)
      (sys_sigaltstack (bytevector-address buf) NULL)))
  ;; Stack used when SA_ONSTACK is set
  (sigaltstack ($processor-data-ref CPU-VECTOR:ALTSIGSTK-BASE)
               ($processor-data-ref CPU-VECTOR:ALTSIGSTK-SIZE)
               0)
  (sigprocmask SIG_BLOCK
               (fxior (fxarithmetic-shift-left 1 (- SIGPIPE 1)) ;not used
                      (fxarithmetic-shift-left 1 (- SIGCHLD 1)) ;signalfd
                      (fxarithmetic-shift-left 1 (- SIGWINCH 1)))) ;signalfd
  (sigaction SIGBUS ($linker-address 'linux:signal-handler) 0 (fxior SA_SIGINFO SA_NODEFER))
  ;; TODO: SIGSEGV should have the alternate stack, too (for stack overflow)
  (sigaction SIGSEGV ($linker-address 'linux:signal-handler) 0 (fxior SA_SIGINFO SA_NODEFER))
  (sigaction SIGFPE ($linker-address 'linux:signal-handler) 0 (fxior SA_SIGINFO SA_NODEFER))
  (sigaction SIGILL ($linker-address 'linux:signal-handler) 0 (fxior SA_SIGINFO SA_NODEFER))
  (sigaction SIGURG ($linker-address 'linux:preempt) 0 (fxior SA_SIGINFO SA_RESTART SA_ONSTACK)))

;; Initialize the terminal (just for the scheduler)
(define (linux-init-terminal)
  ($init-standard-ports (lambda (bv start count)
                          (assert (fx<=? (fx+ start count) (bytevector-length bv)))
                          (sys_read STDIN_FILENO (fx+ (bytevector-address bv) start) count))
                        (lambda (bv start count)
                          (assert (fx<=? (fx+ start count)
                                         (bytevector-length bv)))
                          (sys_write STDOUT_FILENO (fx+ (bytevector-address bv) start) count))
                        (lambda (bv start count)
                          (assert (fx<=? (fx+ start count)
                                         (bytevector-length bv)))
                          (sys_write STDERR_FILENO (fx+ (bytevector-address bv) start) count))
                        (buffer-mode line)
                        (eol-style lf)))

;; Verify that #AC is working and triggers SIGBUS
(define (linux-init-check-alignment)
  (guard (exn (else #f))
    (get-mem-u32 #x200001)              ;safe way to trigger #AC
    (cond
      ((eqv? (valgrind #x1001) 0)
       (display "Fatal: Loko can't run because the system does not support #AC.\n"
                (current-error-port))
       (exit 70))
      (else
       (display "Warning: Valgrind does not emulate #AC, expect trouble!\n"
                (current-error-port))))))

(define (elf-init-process-data)
  (let-values ([(command-line environment-variables auxiliary-vector)
                (elf-parse-stack-data ($boot-loader-data))])
    (init-set! 'command-line command-line)
    (init-set! 'environment-variables environment-variables)
    (init-set! 'auxiliary-vector auxiliary-vector)))

;; Setup for AFL, American Fuzzy Lop.
(define (linux-init-setup-afl)
  (define (afl-fork-server)
    (define FORKSRV_FD 198)
    (define SIZE 4)
    (define NULL 0)
    (let ((tmp (make-bytevector SIZE 0)))
      ;; Tells AFL that the fork server has started.
      (when (eqv? (sys_write (+ FORKSRV_FD 1) (bytevector-address tmp) SIZE
                             (lambda (errno) #f))
                  SIZE)
        (let loop ()
          ;; wait
          (unless (eqv? SIZE (sys_read FORKSRV_FD (bytevector-address tmp) SIZE))
            (exit 2))
          (let ((pid (sys_fork)))
            (cond ((eqv? pid 0)
                   ;; Child.
                   (put-mem-s61 ($linker-address 'afl-location) 0)
                   (sys_close FORKSRV_FD)
                   (sys_close (+ FORKSRV_FD 1)))
                  (else
                   (bytevector-u32-native-set! tmp 0 pid)
                   (sys_write (+ FORKSRV_FD 1) (bytevector-address tmp) SIZE)
                   (sys_wait4 pid (bytevector-address tmp) WUNTRACED NULL)
                   (sys_write (+ FORKSRV_FD 1) (bytevector-address tmp) SIZE)
                   (loop))))))))
  ;; Having "__AFL_SHM_ID" as a bytevector is necessary for AFL to
  ;; recognize that the binary is instrumented.
  (define __AFL_SHM_ID (utf8->string #vu8(95 95 65 70 76 95 83 72 77 95 73 68)))
  (let ((id (cond ((get-environment-variable __AFL_SHM_ID)
                   => string->number)
                  (else #f))))
    (when id
      (guard (exn (else #f))
        (sys_shmat id ($linker-address 'afl-map) SHM_REMAP))
      (afl-fork-server))))

;; Preemption timer
(define (linux-init-preemption-timer)
  ;; Create a timer that sends a SIGURG to the current thread at a
  ;; frequency of 50 Hz, but only counts up while the thread is
  ;; running.
  (let ((freq 1/50)
        (timer (timer-create CLOCK_THREAD_CPUTIME_ID SIGURG)))
    ;; The frequency of the SIGURG signals is at most once per
    ;; second.
    (let ((freq (if (>= freq 1) 999/1000 freq)))
      (let ((seconds 0)
            (nanoseconds (* freq (expt 10 9))))
        (timer-settime timer seconds nanoseconds)))))

(define (linux-init-heap)
  ;; Just a modest little heap for use with dma-allocate/dma-free on
  ;; Linux.
  (define tiny-heap
    (let ((base (* 4 1024 1024 1024))
          (size (* 2 1024 1024)))
      (sys_mmap base size (fxior PROT_READ PROT_WRITE)
                (fxior MAP_PRIVATE MAP_FIXED MAP_ANONYMOUS)
                -1 0)
      (make-buddy base size 12)))
  (list tiny-heap))

(define (linux-init)
  (define (linux-mmap start length type)
    (sys_mmap start length
              (case type
                ((stack heap) (fxior PROT_READ PROT_WRITE))
                #;((trap) (mmap-protection 'PROT_NONE))
                ((text) (fxior PROT_READ PROT_WRITE PROT_EXEC))
                (else (error 'mmap "Unsupported type" type)))
              (case type
                ((stack) (fxior MAP_PRIVATE MAP_FIXED MAP_ANONYMOUS))
                ((heap text) (fxior MAP_PRIVATE MAP_FIXED MAP_ANONYMOUS))
                (else (error 'mmap "Unsupported type" type)))
              -1 0)
    (if #f #f))

  (init-set! 'machine-type '#(amd64 linux))
  (init-set! '$mmap linux-mmap)
  (init-set! 'exit (lambda (status)
                     ;; XXX: status must be a byte?
                     (let lp ()
                       (sys_exit_group (if (fixnum? status)
                                           status
                                           (if (not status) 1 0)))
                       (lp))))
  (linux-init-signal-handlers)
  (linux-init-terminal)
  (linux-init-check-alignment)
  (elf-init-process-data)
  (linux-init-setup-afl)
  (linux-init-preemption-timer)
  (let ((buddies (linux-init-heap)))
    (scheduler-loop 'linux buddies)))

(when (eq? ($boot-loader-type) 'linux)
  (init-set! 'init linux-init)))
