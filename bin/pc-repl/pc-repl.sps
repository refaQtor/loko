#!/usr/bin/env scheme-script
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme PC bare-metal graphical REPL
;; Copyright © 2019-2021 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; REPL that runs on a framebuffer.

;; Sorry for the mess. I'll clean up when I'm done. :)

(import
  (rnrs)
  (rnrs eval)
  (rnrs mutable-strings)
  (only (loko) parameterize make-parameter pretty-print include library-directories
        port-buffer-mode-set!)
  (only (loko system r7rs) current-output-port* current-input-port* current-error-port*)
  (loko match)
  (loko queues)
  (loko system fibers)
  (loko system logging)
  (loko system time)
  (loko system unsafe)

  (only (loko system $primitives) $void?)
  (only (loko runtime repl) banner repl)
  (only (loko system r7rs) current-output-port* current-error-port*)

  (only (psyntax expander) interaction-environment new-interaction-environment)

  (loko drivers pci)
  (loko drivers video bga)
  (loko drivers video vbe)
  (loko font font-6x13)

  (loko drivers mouse)
  (loko drivers keyboard)
  (loko drivers ps2 core)
  (loko drivers ps2 i8042)
  (loko drivers ps2 mouse)
  (loko drivers ps2 keyboard)
  (loko drivers usb hid-numbers)

  (loko drivers usb core)
  (loko drivers usb uhci)
  (loko drivers usb ehci)
  (loko drivers usb hid)
  (loko drivers usb hid-mouse)
  (loko drivers usb hid-keyboard)
  (loko drivers usb mass-storage)

  (loko drivers rtc)
  (loko drivers rtc mc146818)
  (srfi :19 time)

  (loko drivers ata atapi)
  (loko drivers ata core)
  (loko drivers ata drive)
  (loko drivers ata ide)
  (loko drivers ata identify)
  (loko drivers scsi block)
  (loko drivers scsi core)
  (loko drivers storage)

  (loko drivers pci)

  (fs partitions common)
  (fs partitions mbr)
  (fs partitions gpt)
  (fs fatfs)
  (only (loko system $host) install-vfs dma-allocate)

  (loko net internet)
  (loko net dhcpv4-client)
  (loko net tcp)
  (loko drivers net)
  (loko drivers net eepro100)
  (loko drivers net rtl8139)
  (loko drivers net rtl8169)
  (loko drivers net virtio)

  (loko kernel osabi-linux)
  (srfi :98)

  (text-mode console)
  (text-mode console events)
  (text-mode console model))

(define (log/x severity . x*)
  (send-log severity
            (call-with-string-output-port
              (lambda (p)
                (for-each (lambda (x) (display x p)) x*)))))

(define (log/debug . x*) (apply log/x DEBUG x*))
(define (log/info . x*) (apply log/x INFO x*))
(define (log/error . x*) (apply log/x ERROR x*))

(define devs (pci-scan-bus #f))

(define font-h 13)
(define font-w 6)

;;; VBE graphics or Bochs Graphics Array

;; TODO: Make a nice API for getting any kind of fb working, with a
;; mouse. Could be based on the direct rendering manager API, it seems
;; pretty solid.

(define w 1024)
(define h 768)

(define framebuffer
  (cond
    #;
    ((find probe·pci·bga? devs) =>
     (lambda (dev)
       (let ((bga (make·pci·bga dev)))
         (bga-set-mode bga w h 32 #t #t)
         (bga-framebuffer-address bga))))
    ((find probe·pci·vbe? devs) =>
     (lambda (dev)
       (display "Attempting to set a graphics mode with VBE...\n")
       (let ((VBE (make·pci·vbe dev)))
         (define (get-modes vbe)
           (let ((BIOS (vbe-bios vbe))
                 (info (vbe-supervga-info vbe)))
             (map (lambda (mode) (vesa-get-mode-info BIOS mode))
                  (supervga-info-VideoModes info))))
         (let* ((modes (get-modes VBE))
                (mode (find (lambda (info)
                              (and (eqv? (supervga-mode-XResolution info) w)
                                   (eqv? (supervga-mode-YResolution info) h)
                                   (eqv? (supervga-mode-BitsPerPixel info) 32)
                                   (eqv? (supervga-mode-MemoryModel info) 6)))
                            modes)))
           (when (not mode)
             (error #f "Did not find a suitable direct color graphics mode" modes))
           (log/info "Using VBE mode: " mode)
           (vesa-set-mode (vbe-bios VBE) (supervga-mode-Number mode) 'lfb #f #f)
           (let ((modeinfo (vesa-get-mode-info (vbe-bios VBE)
                                               (vesa-get-mode (vbe-bios VBE)))))
             ;; TODO: the mode info has a bunch of more information
             ;; about the layout of the framebuffer that can't just be
             ;; ignored like it is here.
             (supervga-mode-PhysBasePtr modeinfo))))))
    (else
     (error #f "Could not find Bochs or VBE graphics"))))

(define framebuffer-copy
  (let ((&buf (dma-allocate (* w h 4) -1)))
    (do ((i 0 (fx+ i 4)))
        ((fx=? i (* w h 4)))
      (put-mem-u32 (fx+ &buf i) 0))
    &buf))

(define (set-pixel x y c)
  (let ((offset (fx* (fx+ x (fx* y w)) 4)))
    (when (fx<=? 0 offset (fx- (fx* (fx* w h) 4) 4))
      (put-mem-u32 (fx+ framebuffer-copy offset) c))))

;; Mouse pointer by Paer Martinsson
(include "pointer.scm")

(define mouse-x (div w 2))
(define mouse-y (div h 2))
(define mouse-w 32)
(define mouse-h 32)
(define mouse-hx 0)
(define mouse-hy 0)

(define bounding-box                    ;x0,y0--x1,y2
  (vector #f #f #f #f))

(define (extend-bounding-box! x y)
  (let ((x (fxmin (fxmax 0 x) (fx- w 1)))
        (y (fxmin (fxmax 0 y) (fx- h 1))))
    (match bounding-box
      [#(x0 y0 x1 y1)
       (cond ((not x0)
              (vector-set! bounding-box 0 x)
              (vector-set! bounding-box 1 y)
              (vector-set! bounding-box 2 (fx+ x 1))
              (vector-set! bounding-box 3 (fx+ y 1)))
             (else
              (vector-set! bounding-box 0 (fxmin x0 x))
              (vector-set! bounding-box 1 (fxmin y0 y))
              (vector-set! bounding-box 2 (fxmax x1 (fx+ x 1)))
              (vector-set! bounding-box 3 (fxmax y1 (fx+ y 1)))))])))

(define (framebuffer-redraw)
  ;; Redraw all pixels within the bounding box
  (match bounding-box
    [#(x0 y0 x1 y1)
     (when x0
       (do ((y y0 (fx+ y 1))
            (fy (fx* y0 (fx* w 4)) (fx+ fy (fx* w 4))))
           ((fx=? y y1))
         (do ((x x0 (fx+ x 1))
              (idx (fx+ fy (fx* x0 4)) (fx+ idx 4)))
             ((fx=? x x1))
           (put-mem-u32 (fx+ framebuffer idx) (get-mem-u32 (fx+ framebuffer-copy idx)))))
       (vector-set! bounding-box 0 #f))])
  ;; Draw the mouse pointer. Proper graphics hardware usually has some
  ;; special way to do this.
  (do ((y 0 (fx+ y 1)))
      ((fx=? y mouse-h))
    (do ((x 0 (fx+ x 1)))
        ((fx=? x mouse-w))
      (let ((x (fx+ mouse-x x)) (y (fx+ mouse-y y))
            (c (bytevector-u32-ref pointer (fx* 4 (fx+ x (fx* y mouse-w))) (endianness little))))
        (unless (eqv? 0 (fxarithmetic-shift-right c 24)) ;alpha
          (when (and (fx<? -1 x w) (fx<? -1 y h))
            (let ((offset (fx* (fx+ x (fx* y w)) 4)))
              (put-mem-u32 (fx+ framebuffer offset) c))))))))

(define (move-mouse-cursor x y)
  (extend-bounding-box! mouse-x mouse-y)
  (extend-bounding-box! (fx+ mouse-x mouse-w) (fx+ mouse-y mouse-h))
  (set! mouse-x x)
  (set! mouse-y y)
  (extend-bounding-box! mouse-x mouse-y)
  (extend-bounding-box! (fx+ mouse-x mouse-w) (fx+ mouse-y mouse-h))
  (framebuffer-redraw))

;;; text-mode backend for the framebuffer

(define mouse-manager (make-mouse-manager))
(define keyboard-manager (make-keyboard-manager))

(define (manage-ps/2 controller)
  (define hotplug-channel (make-channel))
  (let lp ()
    (match (get-message (PS/2-controller-notify-channel controller))
      [('new-port . port)
       ;; At this point we should probe the port, find a driver for
       ;; the device and spawn a fiber to handle it.
       (let ((reset-id
              (guard (exn ((error? exn) #f))
                (PS/2-command port PS/2-RESET)
                (let ((result (PS/2-read port 4000)))
                  (PS/2-read port 1000)))))
         (cond
           ((probe·PS/2·mouse port)
            => (lambda (id)
                 #;(println (list 'mouse id port))
                 (spawn-fiber (lambda ()
                                (let ((mouse (make-managed-mouse mouse-manager)))
                                  (driver·PS/2·mouse port hotplug-channel id mouse))))))
           ((probe·PS/2·keyboard port)
            => (lambda (id)
                 #;(println (list 'keyboard id port))
                 (spawn-fiber (lambda ()
                                (let ((keyboard (make-managed-keyboard keyboard-manager)))
                                  (driver·PS/2·keyboard port hotplug-channel id keyboard))))))
           (else
            ;; Probing failed, we have no driver. Start the hotplug
            ;; driver, that should tell us when a new device has been
            ;; attached.
            (spawn-fiber (lambda ()
                           (driver·PS/2·hotplug port hotplug-channel))))))])
    (lp)))

;; Convert a text-mode color to a framebuffer color
(define (tm-color->fb-color c) c)

(define (draw-character font x y fg bg ch)
  (extend-bounding-box! x y)
  (extend-bounding-box! (fx+ x font-w) (fx+ y font-h))
  (let lp ((ch ch))
    (let* ((i (char->integer ch))
           (page-num (fxarithmetic-shift-right i 8)))
      (cond ((fx<? page-num (vector-length font))
             (let* ((page (vector-ref font page-num))
                    (bitmap (and (vector? page)
                                 (vector-ref page (fxand i #xff)))))
               (if (bytevector? bitmap)
                   (do ((i 0 (fx+ i 1)))
                       ((fx=? i font-h))
                     (do ((y (fx+ y i))
                          (bitmask (bytevector-u8-ref bitmap i))
                          (xm #x80 (fxarithmetic-shift-right xm 1))
                          (xe (fx+ x font-w))
                          (x x (fx+ x 1)))
                         ((fx=? x xe))
                       (let ((c (if (eqv? 0 (fxand xm bitmask))
                                    (if (fx=? bg Default)
                                        (fxdiv y 4)
                                        bg)
                                    (if (fx=? fg Default)
                                        (rgb-color #xaa #xaa #xaa)
                                        fg))))
                         (set-pixel x y c))))
                   (lp #\nul))))
            (else
             (lp #\nul))))))

(define text-cursor-color Gray)

(define text-mode-event-ch (make-channel))

(define (framebuffer-textmode-backend)
  (define cols (fxdiv w font-w))
  (define rows (fxdiv h font-h))
  (lambda (cmd arg)
    (case cmd
      [(get-size)
       (values cols rows w h)]
      [(init)
       #f]
      [(update redraw)
       (let* ((c arg)
              ;; Cursor location in absolute coordinates
              (cx (fx+ (console-x c) (console-x1 c)))
              (cy (fx+ (console-y c) (console-y1 c))))
         (assert (fx=? cols (console-full-cols c)))
         (assert (fx=? rows (console-full-rows c)))
         (do ((y 0 (fx+ y 1))) ((fx=? y rows))
           (let ((ch-y (fx* font-h y)))
             (when (console-row-dirty? c y 'absolute)
               (let-values ([(buf mbuf fgbuf bgbuf abuf idx) (%index/nowin c 0 y)])
                 (do ((x 0 (fx+ x 1))) ((fx=? x cols))
                   (if (and (eqv? x cx) (eqv? y cy))
                       (draw-character font (fx* font-w cx) ch-y
                                       (rgb-color 0 0 0) text-cursor-color
                                       #\space)
                       ;; TODO: Bold, italics, etc
                       (let ((ch (text-ref buf (fx+ idx x))))
                         (unless (textcell-unused? ch)
                           (let ((fg (tm-color->fb-color (fg-ref fgbuf (fx+ idx x))))
                                 (bg (tm-color->fb-color (bg-ref bgbuf (fx+ idx x))))
                                 (ch-x (fx* font-w x)))
                             (draw-character font ch-x ch-y fg bg ch))))))))))
         (clear-console-dirty! c)
         (set-console-dirty! c (console-y c))
         (framebuffer-redraw))]
      [(read-event)
       (get-message text-mode-event-ch)]
      [else
       #f])))

(define (text-mode-keyboard&mouse-driver)
  (define (hid->key-symbol page usage)
    ;; FIXME: recover the key location
    (case page
      ((#x07)
       (cond ((assv usage
                    '((#x28 . Enter)
                      (#x2a . Backspace)
                      (#x2b . Tab)
                      (#x4a . Home)
                      (#x4b . PageUp)
                      (#x4c . Delete)
                      (#x4d . End)
                      (#x4e . PageDown)
                      (#x4f . ArrowRight)
                      (#x50 . ArrowLeft)
                      (#x51 . ArrowDown)
                      (#x52 . ArrowUp)))
              => cdr)
             (else #f)))
      (else #F)))
  (define (hid->modifier-set leds mods)
    (define (fxtest? mods mask)
      (not (eqv? 0 (fxand mods mask))))
    ((enum-set-constructor (modifier-set))
     (filter values
             (list
              (and (fxtest? mods HID-modifiers-Control) (modifier ctrl))
              (and (fxtest? mods HID-modifiers-Shift) (modifier shift))
              (and (fxtest? mods HID-modifiers-Alt) (modifier alt))
              (and (fxtest? mods HID-modifiers-GUI) (modifier meta))
              #;(and (fxtest? mods HID-modifiers-AltGr) (modifier alt-graph))
              #;(and (fxbit-set? leds HID-LED-Caps-Lock) (modifier caps-lock))
              #;(and (fxbit-set? leds HID-LED-Num-Lock) (modifier num-lock))
              #;(and (fxbit-set? leds HID-LED-Scroll-Lock) (modifier scroll-lock))))))
  (define mouse-scale 1)
  (let lp ()
    ;; XXX: It is tempting to separate the keyboard and the mouse, but
    ;; the user can hold modifiers on the keyboard while using the
    ;; mouse
    (match (perform-operation
            (choice-operation
             (wrap-operation (get-operation
                              (mouse-event-channel mouse-manager))
                             (lambda (x) (cons 'mouse x)))
             (wrap-operation (get-operation
                              (keyboard-event-channel keyboard-manager))
                             (lambda (x) (cons 'kbd x)))))
      [('mouse . #(xd yd zd buttons _))
       (let ((x (fxmax 0 (fxmin (* mouse-scale (fx- w 1)) (fx+ mouse-x xd))))
             (y (fxmax 0 (fxmin (* mouse-scale (fx- h 1)) (fx+ mouse-y yd)))))
         ;;(println (list x y 'mouse xd yd buttons))
         (move-mouse-cursor x y)
         (lp))]
      [('kbd . #(make/break _ page usage (leds mods key)))
       (let ((modifiers (hid->modifier-set leds mods)))
         (when (eq? make/break 'make)
           (cond
             ((hid->key-symbol page usage) =>
              (lambda (keysym)
                (put-message text-mode-event-ch
                             (make-key-press-event modifiers key keysym 'standard))))
             ((char? key)
              (put-message text-mode-event-ch
                           (make-key-press-event modifiers key key 'standard)))
             (else
              (put-message text-mode-event-ch
                           (make-unknown-event 'keyboard (list page usage key)))))))
       (lp)]
      [('kbd . event)
       (put-message text-mode-event-ch (make-unknown-event 'keyboard event))
       (lp)])))

(current-console (make-console (framebuffer-textmode-backend)))

;;; the repl

(define (buffer-channel in-ch out-ch len)
  (define q (make-queue))
  (assert (and (channel? in-ch) (channel? out-ch) (fixnum? len)))
  (spawn-fiber
   (lambda ()
     (let lp ((qlen 0))
       (match (perform-operation (choice-operation
                                  (wrap-operation (get-operation in-ch)
                                                  (lambda (x) (cons 'get x)))
                                  (if (queue-empty? q)
                                      (choice-operation)
                                      (wrap-operation (put-operation out-ch (queue-front q))
                                                      (lambda _ 'put)))))
         [('get . msg)
          (cond ((fx>? qlen len)
                 ;; Drop the oldest message.
                 (dequeue! q)
                 (enqueue! q msg)
                 (lp qlen))
                (else
                 (enqueue! q msg)
                 (lp (fx+ qlen 1))))]
         ['put
          (dequeue! q)
          (lp (fx- qlen 1))])))))

(define (println/newlines str)
  (let ((p (open-string-input-port str)))
    (let lp ()
      (let ((line (get-line p)))
        (unless (eof-object? line)
          (println line)
          (lp))))))

(define (print/write datum)
  (print
   (call-with-string-output-port
     (lambda (p) (write datum p)))))

;; TODO: Support styles on the standard implementation of print-condition
(define (print-condition exn)
  (cond ((condition? exn)
         (let ((c* (simple-conditions exn)))
           (text-color LightRed)
           (println "An unhandled condition was raised:")
           (do ((i 1 (fx+ i 1))
                (c* c* (cdr c*)))
               ((null? c*))
             (let* ((c (car c*))
                    (rtd (record-rtd c)))
               (text-color Default)
               (print " ")
               (let loop ((rtd rtd))
                 (text-attribute 'bold (eq? rtd (record-rtd c)))
                 (print (record-type-name rtd))
                 (text-attribute 'bold #f)
                 (cond ((record-type-parent rtd) =>
                        (lambda (rtd)
                          (unless (eq? rtd (record-type-descriptor &condition))
                            (print " ")
                            (loop rtd))))))
               (let loop ((rtd rtd))
                 (do ((f* (record-type-field-names rtd))
                      (i 0 (fx+ i 1)))
                     ((fx=? i (vector-length f*))
                      (cond ((record-type-parent rtd) => loop)))
                   (println)
                   (print "  ")
                   (text-color Default)
                   (text-attribute 'italic #t)
                   (print (vector-ref f* i))
                   (print ": ")
                   (text-attribute 'italic #f)
                   (let ((x ((record-accessor rtd i) c)))
                     (cond ((and (eq? rtd (record-type-descriptor &irritants))
                                 (pair? x) (list? x))
                            (print "(")
                            (let ((list-x (wherex)))
                              (text-color LightRed)
                              (print/write (car x))
                              (for-each (lambda (value)
                                          (println)
                                          (gotoxy list-x (wherey))
                                          (print/write value))
                                        (cdr x)))
                            (text-color Default)
                            (print ")"))
                           (else
                            (text-color LightRed)
                            (print/write x)))))))
             (println))))
        (else
         (println "A non-condition object was raised:")
         (print/write exn))))

(define tui-output-ch (make-channel))

(define (make-console-output-port color bufmode)
  (define id "console")
  (define (write! str start count)
    ;; TODO: Parse ANSI codes
    (put-message tui-output-ch (list color (substring str start (+ start (min 200 count)))))
    count)
  (letrec ((close
            (lambda ()
              ;; TODO: Maybe it's better to allow this and then repair it
              (error 'close-port "Closing the console ports is not permitted" p)))
           (p (make-custom-textual-output-port id write! #f #f close)))
    (port-buffer-mode-set! p bufmode)
    p))

(current-output-port* (make-console-output-port Default (buffer-mode line)))
(current-error-port* (make-console-output-port LightRed (buffer-mode none)))

(define (eval-expr datum env)
  (call/cc
    (lambda (k)
      (with-exception-handler
        (lambda (exn)
          (send-log WARNING
                    (call-with-string-output-port
                      (lambda (p)
                        (display "Exception from the expression " p)
                        (write datum p)))
                    'EXCEPTION exn)
          (when (serious-condition? exn)
            (k #t)))
        (lambda ()
          (call-with-values
            (lambda ()
              (let-values ((v* (eval datum env)))
                (flush-output-port (current-output-port))
                (flush-output-port (current-error-port))
                (apply values v*)))
            (case-lambda
              ((x)
               (unless ($void? x)
                 (pretty-print x)))
              (() #f)
              (x*
               (for-each pretty-print x*))))
          (flush-output-port (current-output-port))
          (flush-output-port (current-error-port)))))))

(define (spawn-program cmdline)
  ;; XXX: This is a pretty cheap command line parser that's just meant
  ;; to be used to launch some initial program which then does more
  ;; advanced shell stuff.
  (define (parse-cmdline cmdline)
    (let ((p (open-string-input-port cmdline)))
      (let f ()
        (let-values ([(strp extract) (open-string-output-port)])
          (let lp ((prev #f))
            (let ((c (get-char p)))
              (cond ((eof-object? c)
                     (if prev
                         (list (extract))
                         '()))
                    ((char-whitespace? c)
                     (cond
                       ((not prev)
                        (lp prev))
                       ((char-whitespace? prev)
                        (lp c))
                       (else
                        (cons (extract) (f)))))
                    ((eqv? c #\")
                     (let lp* ()
                       (let ((c (get-char p)))
                         (cond ((eof-object? c)
                                (error 'spawn-program "Unterminated quote" cmdline))
                               ((eqv? c #\")
                                (cons (extract) (f)))
                               (else
                                (put-char strp c)
                                (lp*))))))
                    (else
                     (put-char strp c)
                     (lp c)))))))))
  (guard (exn
          ((serious-condition? exn)
           (send-log ERROR "Command start failed"
                     'EXCEPTION exn)))
    (let ((command-line (parse-cmdline cmdline)))
      (when (null? command-line)
        (error 'spawn-program "Empty command line" cmdline))
      (spawn-linux-process (car command-line)
                           (map string->utf8 command-line)
                           (map (lambda (var)
                                  (string->utf8
                                   (string-append (car var) "=" (cdr var))))
                                (get-environment-variables))))))

(define env (make-parameter #f))

(define (tui)
  (define log-ch (make-channel))
  (define event-ch (make-channel))
  (define line "")
  (define cur 0)
  (define prev #f)
  (define prompty (wherey))
  (define outputx 0)
  (define outputy (wherey))
  (define visible-severity INFO)

  (define (redraw-prompt)
    (gotoxy 0 prompty)
    (text-color LightGreen)
    (print "> ")
    (let ((prompt-width (wherex)))
      (text-color Default)
      (print line)
      (clreol)
      (gotoxy (fx+ cur prompt-width) (wherey))))

  ;; Receive logs, with buffering
  (current-log-callback
   (let ((logw-ch (make-channel)))
     (buffer-channel logw-ch log-ch 100)
     (lambda (e) (put-message logw-ch e))))
  ;; Receive text-mode events
  (spawn-fiber (lambda ()
                 (let lp ()
                   (put-message event-ch (read-event))
                   (lp))))

  ;; TODO: This process should receive messages for windows, wait for
  ;; input and dispatch it to either another process or the window
  ;; handling, and output screen updates.

  (redraw-prompt)

  (unless (interaction-environment)
    (interaction-environment (new-interaction-environment)))
  (do ((stop #f)) (stop)
    (match (perform-operation
            (choice-operation
             (wrap-operation (get-operation log-ch) (lambda (x) (cons 'log x)))
             (wrap-operation (get-operation event-ch) (lambda (x) (cons 'event x)))
             (wrap-operation (get-operation tui-output-ch) (lambda (x) (cons 'output x)))))
      ;; An input event
      [('event . ev)

       (set! text-cursor-color Gray)

       (when #f
         ;; Debug event handling
         (cond
           ((key-press-event? ev)
            (log/debug (list 'press (enum-set->list (keyboard-event-mods ev))
                             (keyboard-event-char ev) (keyboard-event-key ev)
                             (keyboard-event-location ev))))
           ((unknown-event? ev)
            (log/debug (list 'unknown (unknown-event-source ev)
                             (if (integer? (unknown-event-data ev))
                                 (number->string (unknown-event-data ev) 16)
                                 (unknown-event-data ev)))))
           (else
            (log/debug ev))))

       (cond
         ((and (key-press-event? ev)
               (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
               (eqv? (keyboard-event-key ev) #\l))
          (clrscr))

         ((resize-event? ev)
          #f)

         ((and (key-press-event? ev)
               (or (and
                     (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                     (eqv? (keyboard-event-key ev) #\a))
                   (eq? (keyboard-event-key ev) 'Home)))
          (set! cur 0))

         ((and (key-press-event? ev)
               (or (and
                     (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                     (eqv? (keyboard-event-key ev) #\e))
                   (eq? (keyboard-event-key ev) 'End)))
          (set! cur (string-length line)))

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'Backspace)
               (enum-set=? (enum-set-difference (keyboard-event-mods ev)
                                                (modifier-set shift))
                           (modifier-set)))
          (unless (eqv? cur 0)
            (set! line (string-append (substring line 0 (fx- cur 1))
                                      (substring line cur (string-length line))))
            (set! cur (fx- cur 1))))

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowLeft)
               (not (keyboard-event-has-modifiers? ev)))
          (unless (eqv? cur 0)
            (set! cur (fx- cur 1))))

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowRight)
               (not (keyboard-event-has-modifiers? ev)))
          (unless (eqv? cur (string-length line))
            (set! cur (fx+ cur 1))))

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'ArrowUp)
               (not (keyboard-event-has-modifiers? ev)))
          (set! line prev)
          (set! cur (string-length line)))

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'Tab)
               (not (keyboard-event-has-modifiers? ev)))
          #f)

         ((and (key-press-event? ev) (eq? (keyboard-event-key ev) 'Enter)
               (not (keyboard-event-has-modifiers? ev)))
          (cond
            ((equal? line ""))

            ((char=? (string-ref line 0) #\@)
             ;; Program invocation
             (set! prev line)
             (let ((cmdline (substring line 1 (string-length line))))
               (set! line "")
               (set! cur 0)
               (println)
               (clreol)
               (set! prompty (wherey))
               (set! outputx 0)
               (set! outputy (wherey))
               (spawn-program cmdline)))

            (else
             ;; Scheme expression
             (set! prev line)
             (call/cc
               (lambda (k)
                 (guard (exn ((lexical-violation? exn)
                              (set! text-cursor-color LightRed)
                              (k #t)))
                   (let* ((line-p (open-string-input-port line))
                          (datum (read line-p)))
                     (set! line (if (port-eof? line-p) "" (get-string-all line-p)))
                     (set! cur 0)
                     (println)
                     (clreol)
                     (set! prompty (wherey))
                     (set! outputx 0)
                     (set! outputy (wherey))
                     (spawn-fiber
                      (lambda ()
                        (eval-expr datum (interaction-environment))
                        (update-screen+time))))))))))

         ((and (key-press-event? ev) (eq? (keyboard-event-char ev) #\+)
               (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl)))
          (unless (eqv? visible-severity DEBUG)
            (set! visible-severity (fx+ visible-severity 1))))

         ((and (key-press-event? ev) (eqv? (keyboard-event-char ev) #\-)
               (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl)))
          (unless (eqv? visible-severity EMERGENCY)
            (set! visible-severity (fx- visible-severity 1))))

         ((and (key-press-event? ev) (keyboard-event-char ev)) =>
          (lambda (char)
            (set! line (string-append (substring line 0 cur)
                                      (string char)
                                      (substring line cur (string-length line))))
            (set! cur (fx+ cur 1)))))

       ;; Update the screen immediately after handling user input
       (redraw-prompt)
       (update-screen+time)]

      [('log . e)
       (let ((severity (cdr (assq 'SEVERITY e)))
             (message (cdr (assq 'MESSAGE e)))
             (subsystem (cond ((assq 'SUBSYSTEM e) => cdr)
                              (else #f))))
         (when (fx<=? severity visible-severity)
           (gotoxy 0 prompty)
           (clreol)
           (gotoxy outputx outputy)

           (cond ((assq 'JIFFY e) =>
                  (lambda (x)
                    (text-color Gray)
                    (print (cdr x))
                    (print " "))))
           (when subsystem
             (text-color DarkGray)
             (print subsystem)
             (print " "))
           (text-color Default)
           (print "[")
           (cond ((eqv? severity DEBUG) (text-color White))
                 ((eqv? severity INFO) (text-color LightGreen))
                 ((eqv? severity ERROR) (text-color LightRed))
                 ((eqv? severity WARNING) (text-color Yellow))
                 ((eqv? severity CRITICAL) (text-color LightMagenta))
                 (else (text-color Default)))
           (print (severity->symbol severity))
           (text-color Default)
           (print "] ")
           (println message)
           (cond ((assq 'EXCEPTION e) =>
                  (lambda (exn)
                    (print-condition (cdr exn)))))
           (set! outputx (wherex))
           (set! outputy (wherey))
           (unless (eqv? 0 outputx)
             (println))

           (set! prompty (wherey))))
       (redraw-prompt)]

      [('output color str)
       (gotoxy 0 prompty)
       (clreol)
       (gotoxy outputx outputy)
       (text-color color)
       (do ((buf (make-string 1))
            (i 0 (fx+ i 1)))
           ((fx=? i (string-length str)))
         (let ((c (string-ref str i)))
           (cond ((eqv? c #\newline)
                  (println))
                 (else
                  ;; XXX: The text-mode API is a bit unfortunate for
                  ;; this particular use case.
                  (string-set! buf 0 c)
                  (print buf)))))
       (set! outputx (wherex))
       (set! outputy (wherey))
       (unless (eqv? 0 outputx)
         (println))
       (set! prompty (wherey))
       (redraw-prompt)])))

(define (print-lines string)
  (let ((p (open-string-input-port string)))
    (let lp ()
      (let ((line (get-line p)))
        (unless (eof-object? line)
          (println line)
          (lp))))))

(clrscr)
(gotoxy 0 0)
(text-color Gray)
(print-lines (call-with-string-output-port banner))
(update-screen)

(spawn-fiber
 (lambda ()
   (let ((controller (make-PS/2-controller)))
     (spawn-fiber (lambda () (manage-ps/2 controller)))
     (driver·isa·i8042 controller))))

(spawn-fiber text-mode-keyboard&mouse-driver)

(spawn-fiber tui)

;;; Screen refresh

(define update-screen+time
  (let ((prev-time ""))
    (lambda ()
      ;; Always show the date and time in the upper-left corner of the screen
      (let ((now (current-date)))
        (let ((x (wherex)) (y (wherey))
              (str (if (not now)
                       "????-??-?? ??:?? UTC"
                       (date->string now "~Y-~m-~d ~H:~M UTC"))))
          (unless (string=? prev-time str)
            (set! prev-time str)
            (gotoxy (fx- (window-maxx) (string-length str)) 0)
            (text-color Yellow)
            (print str)
            (gotoxy x y))))
      ;; Get the screen contents onto the framebuffer
      (update-screen))))

(define (screen-refresh)
  (extend-bounding-box! 0 0)
  (extend-bounding-box! w h)
  (let lp ()
    (update-screen+time)
    (sleep 1/10)
    (lp)))

;;; Real-time clock

(let ((rtc (make-rtc-device)))
  (spawn-fiber
   (lambda ()
     (driver·rtc·mc146818 rtc)))
  (spawn-fiber
   (lambda ()
     ;; Update the system clock from the RTC once at boot. (It's
     ;; certainly possible to do much better than this, but then you
     ;; should look in to how NTP works).
     (let retry ()
       (let-values ([(now ticks) (rtc-device-get-date rtc)])
         (cond (now
                (set-current-time/utc (date->time-utc now) ticks))
               (else
                (sleep 0.5)
                (retry))))))))

;;; USB


(define (find-hid-driver hiddev)
  (cond
    ((probe·usb·hid·mouse? hiddev)
     (let ((mouse (make-managed-mouse mouse-manager)))
       (driver·usb·hid·mouse hiddev mouse)))
    ((probe·usb·hid·keyboard? hiddev)
     (let ((keyboard (make-managed-keyboard keyboard-manager)))
       (driver·usb·hid·keyboard hiddev keyboard)))
    (else
     (let lp ()
       (log/debug "Unknown HID data: "
                  (perform-operation (usb-pipe-in-operation (usbhid-device-in-pipe hiddev))))
       (lp)))))

(define (manage-usb-hci controller)
  (let lp ()
    (match (get-message (usb-controller-notify-channel controller))
      [('new-device . dev)
       ;; The device configuration is already set. At this point we
       ;; should find a driver for the device and spawn a fiber to
       ;; handle it.
       (let* ((desc (usb-get-device-descriptor dev))
              (ver (devdesc-bcdUSB desc))
              (bcddev (devdesc-bcdDevice desc)))
         (log/info "Found a USB " (number->string (fxbit-field ver 8 16) 16) "."
                   (number->string (fxbit-field ver 0 8) 16)
                   " device with class "
                   (number->string (devdesc-bDeviceClass desc) 16) "h/"
                   (number->string (devdesc-bDeviceSubClass desc) 16) "h/"
                   (number->string (devdesc-bDeviceProtocol desc) 16) "h"
                   ", vendor #x" (number->string (devdesc-idVendor desc) 16)
                   ", product #x" (number->string (devdesc-idProduct desc) 16)
                   ", device " (number->string (fxbit-field bcddev 8 16))
                   "." (number->string (fxbit-field bcddev 0 8) 16)
                   ", strings: " (usb-device-$strings dev)))

       (log/debug (call-with-string-output-port
                    (lambda (p)
                      (print-usb-descriptor (usb-get-device-descriptor dev) p)
                      (for-each (lambda (cfgdesc*)
                                  (for-each (lambda (desc) (print-usb-descriptor desc p))
                                            cfgdesc*))
                                (usb-device-$configurations dev)))))

       (for-each (lambda (interface)
                   (cond
                     ((probe·usb·mass-storage? dev interface)
                      (spawn-fiber
                       (lambda ()
                         (define (get-scsi-req-channel)
                           (let ((scsi-req-ch (make-channel)))
                             (put-message scsi-manager-ch (cons 'new-device scsi-req-ch))
                             scsi-req-ch))
                         (driver·usb·mass-storage dev interface get-scsi-req-channel))))

                     ((probe·usb·hid? dev interface)
                      (spawn-fiber
                       (lambda ()
                         (driver·usb·hid dev interface find-hid-driver))))))
                 (usb-device-interfaces dev))])
    (lp)))

;;; Disk stuff

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

(define (log-device-info device-type identify-block)
  (let-values ([(logical physical) (ata-identify:ata-sector-size identify-block)])
    (log/info "Found an " device-type " drive with model " (ata-identify:model-number identify-block)
              ", serial " (ata-identify:serial-number identify-block)
              ", firmware " (ata-identify:firmware-revision identify-block)
              ", commands: " (ata-identify:supported-command-set identify-block)
              ;; " Major-version: " (ata-identify:major-revision identify-block)
              (cond ((ata-identify:ata-device? identify-block) ", ATA: ")
                    ((ata-identify:atapi-device? identify-block) ", ATAPI: ")
                    (else ", Unknown device: "))
              (if (ata-identify:ata-device? identify-block)
                  (list "Logical sector size:" logical
                        "Physical sector size:" physical)
                  (list)))
    #;
    (log/info "Raw identify block: " identify-block)))

(define storage-manager-ch (make-channel))

(define (make-sector-reader storage lba-start lba-end)
  (lambda (lba)
    (cond
      ((or (not lba-end) (<= lba-start lba lba-end))
       (let ((resp-ch (make-channel)))
         (put-message (storage-device-request-channel storage)
                      (list resp-ch 'read (+ lba-start lba) 1))
         (match (get-message resp-ch)
           [('ok data)
            data]
           [('error . x)
            (log/error "Error reading from medium: LBA=" lba " error=" x)
            (eof-object)])))
      (else
       (log/error "Out of range read")
       (eof-object)))))

(define (storage-device->fatfs-device storage-device starting-lba ending-lba)
  (define logical-sector-size
    (storage-device-logical-sector-size storage-device))
  (define (read-sectors lba sectors)
    (assert (and (fx>=? lba 0) (fx<=? 1 sectors 1024)))
    (let ((resp-ch (make-channel)))
      (let* ((read-lba (+ lba starting-lba))
             (read-end-lba (+ read-lba sectors)))
        (unless (or (not ending-lba) (<= read-end-lba ending-lba))
          (assertion-violation #f "Out of range storage device read"
                               storage-device lba))
        (put-message (storage-device-request-channel storage-device)
                     (list resp-ch 'read read-lba sectors)))
      (match (get-message resp-ch)
        [('ok data) data]
        [('error . x) (error #f "Read error" storage-device x)])))
  (define (write-sectors lba data)
    (let ((resp-ch (make-channel)))
      (let* ((sectors (fxdiv (bytevector-length data) logical-sector-size))
             (write-lba (+ lba starting-lba))
             (write-end-lba (+ write-lba sectors)))
        (unless (or (not ending-lba) (<= write-end-lba ending-lba))
          (assertion-violation #f "Out of range storage device read"
                               storage-device lba))
        (put-message (storage-device-request-channel storage-device)
                     (list resp-ch 'write write-lba data)))
      (match (get-message resp-ch)
        [('ok) (if #f #f)]
        [('error . x)
         (error #f "Write error" storage-device x)])))
  (define (flush) #f)
  (define (close) #f)
  (make-fatfs-device logical-sector-size
                     read-sectors
                     write-sectors
                     flush
                     close))

(define (fatfs-directory-files fs dirname)
  (let ((dir (fatfs-open-directory fs dirname)))
    (let lp ((ret '()))
      (let ((info (fatfs-read-directory dir)))
        (cond ((eof-object? info)
               (fatfs-close-directory dir)
               (reverse ret))
              (else
               (lp (cons (or (fatfs-file-info:long-filename info)
                             (fatfs-file-info:filename info))
                         ret))))))))

(define (manage-storage)
  (let lp ()
    (match (get-message storage-manager-ch)
      [('new-storage storage)
       (log/debug "New storage device: " storage)
       (spawn-fiber
        (lambda ()
          ;; Scan the partition table and list root directories of FAT
          ;; filesystems. Skip 2048-byte sector drives for now, they
          ;; are CD drives.
          (unless (eqv? (storage-device-logical-sector-size storage) 2048)
            (let ((read-sector (make-sector-reader storage 0 #f))
                  (sector-size (storage-device-logical-sector-size storage)))
              (let ((mbr (read-mbr read-sector)))
                ;; XXX: Real applications should probably check the MBR
                ;; to verify that it's a protective MBR. It could have
                ;; been overwritten with a valid MBR, in which case
                ;; gpt1 is likely an out of date GPT backup.
                (let-values ([(gpt0 gpt1) (read-gpt read-sector #f)])
                  (log/info "MBR: " mbr)
                  (when (parttable-valid? gpt0)
                    (log/info "GPT0: " gpt0))
                  (when (parttable-valid? gpt1)
                    (log/info "GPT1: " gpt1))
                  (let ((table (cond ((parttable-valid? gpt0) gpt0)
                                     ((parttable-valid? gpt1) gpt1)
                                     (else mbr))))
                    (for-each
                     (lambda (part)
                       (define GPT-TYPE-EFI-System
                         #vu8(#xC1 #x2A #x73 #x28
                                   #xF8 #x1F #x11 #xD2 #xBA #x4B
                                   #x00 #xA0 #xC9 #x3E #xC9 #x3B))
                       (when (or (member (part-type part)
                                         '(1 4 6 #xb #xc #xd #xe #xef))
                                 (equal? (part-type part) GPT-TYPE-EFI-System))
                         (log/info "Mounting the FAT file system on " part)
                         (let* ((dev (storage-device->fatfs-device storage
                                                                   (part-start-lba part)
                                                                   (part-end-lba part)))
                                (fs (open-fatfs dev)))
                           (define (parse-filename who filename)
                             (if (string=? filename "/")
                                 '()
                                 (let ((components (string-split filename #\/)))
                                   (cond ((and (not (string=? filename ""))
                                               (char=? (string-ref filename 0) #\/))
                                          (cdr components))
                                         (else
                                          (condition
                                           (make-i/o-filename-error filename)
                                           (make-who-condition who)
                                           (make-message-condition "Bad filename")))))))
                           (define (open-file filename file-options buffer-mode who)
                             (define no-create (enum-set-member? 'no-create file-options))
                             (define no-fail (enum-set-member? 'no-fail file-options))
                             (define no-truncate (enum-set-member? 'no-truncate file-options))
                             (define create (not no-create))
                             (define fail (not no-fail))
                             (define truncate (not no-truncate))
                             ;; (log/info (list filename file-options buffer-mode who))
                             (let ((path/err (parse-filename 'open-file filename))
                                   (flags (case who
                                            ((open-file-output-port open-file-input/output-port)
                                             (if (and fail create)
                                                 (fxior fatfs-open/create fatfs-open/exclusive)
                                                 (if truncate
                                                     (if (and no-fail create)
                                                         (fxior fatfs-open/create fatfs-open/truncate)
                                                         fatfs-open/truncate)
                                                     (if (and no-fail create)
                                                         fatfs-open/create
                                                         0))))
                                            (else 0)))
                                   (access-mode (case who
                                                  ((open-file-input-port) fatfs-open/read)
                                                  ((open-file-output-port) fatfs-open/write)
                                                  ((open-file-input/output-port) fatfs-open/read+write))))
                               (if (or (pair? path/err) (null? path/err))
                                   (guard (exn
                                           ((and (who-condition? exn)
                                                 (eq? (condition-who exn) 'fatfs-open-file))
                                            (raise
                                              (condition exn (make-i/o-filename-error filename)))))
                                     (fatfs-open-file fs path/err (fxior access-mode flags)))
                                   (raise
                                     (condition path/err
                                                (make-irritants-condition
                                                 (list filename file-options buffer-mode)))))))
                           (define (file-exists? fn)
                             (let ((path/err (parse-filename 'file-exists? fn)))
                               (if (or (pair? path/err) (null? path/err))
                                   (fatfs-file-exists? fs path/err)
                                   (raise path/err))))
                           (define (directory-files fn hidden?)
                             (let ((path/err (parse-filename 'directory-files fn)))
                               (if (or (pair? path/err) (null? path/err))
                                   (fatfs-directory-files fs path/err)
                                   (raise path/err))))
                           (log/debug fs)
                           (log/info "FAT root directory: "
                                     (fatfs-directory-files fs '()))
                           (install-vfs 'open-file open-file
                                        'file-exists? file-exists?
                                        'directory-files directory-files
                                        )
                           (library-directories (cons "/lib" (library-directories))))))
                     (parttable-partitions table)))))))))])
    (lp)))

(spawn-fiber (lambda ()
               (parameterize ([current-log-fields
                               (append '(SUBSYSTEM "storage")
                                       (current-log-fields))])
                 (manage-storage))))

(define scsi-manager-ch (make-channel))
(spawn-fiber
 (lambda ()
   (let lp ()
     (match (get-message scsi-manager-ch)
       [('new-device . ch)
        (spawn-fiber
         (lambda ()
           (match (probe·scsi ch)
             [((and (or 'SBC 'MMC) class) inq vpd-id)
              (log/info "Found a SCSI SBC/MMC compatible device."
                        " INQUIRY: " inq " VPD ID: " vpd-id)
              (let* ((scsidev (make-scsi-device ch inq))
                     (storage (let-values ([(max-lba sector-size)
                                            (scsi·block·read-capacity scsidev)])
                                ;; FIXME: this can't be the right way?
                                (let ((sector-size (or sector-size
                                                       (case class
                                                         ((MMC) 2048)
                                                         (else 512)))))
                                  (make-storage-device "SCSI logical unit" sector-size)))))
                (put-message storage-manager-ch (list 'new-storage storage))
                (driver·scsi·block scsidev storage))]
             [(_ inq vpd-id)
              (log/info "No driver for this SCSI device."
                        " INQUIRY: " inq " VPD ID: " vpd-id)])))])
     (lp))))

(define (ata-manager controller)
  (let lp ()
    (match (get-message (ata-controller-notify-channel controller))
      [('new-device . ch)
       ;; Probe the device and start a driver
       (spawn-fiber
        (lambda ()
          (match (probe·ata ch)
            [('ata . identify-block)
             (log-device-info 'ATA identify-block)
             (let-values ([(logical _) (ata-identify:ata-sector-size identify-block)])
               (let* ((atadev (make-ata-device controller ch identify-block))
                      (storage (make-storage-device "ATA drive" logical)))
                 (put-message storage-manager-ch (list 'new-storage storage))
                 (driver·ata·drive atadev storage)))]

            [('atapi . identify-block)
             (log-device-info 'ATAPI identify-block)
             (let ((scsi-req-ch (make-channel)))
               (put-message scsi-manager-ch (cons 'new-device scsi-req-ch))
               (driver·ata·atapi (make-ata-device controller ch identify-block)
                                 scsi-req-ch))]

            ['no-device
             '(log/info "No device")]

            [(type . x)
             (log/info "No driver for " type " devices")])))
       (lp)])))


(for-each (lambda (dev)
            (when (probe·pci·ide? dev)
              (log/info "Found an IDE controller: " dev)
              (let ((controller (make-ata-controller)))
                (spawn-fiber (lambda () (ata-manager controller)))
                (spawn-fiber (lambda () (driver·pci·ide dev controller))))))
          devs)

(define (start-networking iface)
  (let* ((ether (make-ether iface))
         (tcp (make-tcp ether)))
    (spawn-fiber (lambda () (net·ethernet·internet-stack ether)))
    (spawn-fiber (lambda ()
                   (parameterize ([current-log-fields
                                   (append '(SUBSYSTEM "dhcpv4")
                                           (current-log-fields))])
                     (net·ethernet·dhcpv4-client ether))))
    (spawn-fiber (lambda () (net·tcp/ip tcp)))
    (spawn-fiber
     (lambda ()
       ;; Wait for an IP address
       (let lp ()
         (sleep 1)
         (unless (ether-default-ipv4-source ether)
           (lp)))
       ;; Listen to the telnet port and start a repl when anyone
       ;; connects.
       (let* ((local-addr (ether-default-ipv4-source ether))
              (local-port 23)
              (telnet-server (tcp-open tcp local-addr local-port '* '* 'passive)))
         (log/info "You may now connect to port 23 to access the remote REPL")
         (let lp ()
           (let-values ([(conn addresses) (tcp-accept telnet-server)])
             (log/info "Accepting REPL connection from " addresses)
             (spawn-fiber
              (lambda ()
                (let* ((tc (make-transcoder (utf-8-codec) (eol-style crlf)))
                       (pi (transcoded-port (tcp-conn-input-port conn addresses) tc))
                       (po (transcoded-port (tcp-conn-output-port conn addresses #f) tc))
                       (pe (transcoded-port (tcp-conn-output-port conn addresses #t) tc)))
                  (parameterize ([current-input-port* pi]
                                 [current-output-port* po]
                                 [current-error-port* pe])
                    (banner (current-output-port))
                    (repl)))))
             (lp))))))))

(for-each (lambda (dev)
            (cond ((probe·pci·virtio-net? dev)
                   (log/info "Found a virtio network card: " dev)
                   (let ((iface (make-iface 'ethernet)))
                     (spawn-fiber (lambda () (driver·pci·virtio-net dev iface)))
                     (start-networking iface)))
                  ((probe·pci·rtl8139? dev)
                   (log/info "Found an rtl8139 network card: " dev)
                   (let ((iface (make-iface 'ethernet)))
                     (spawn-fiber (lambda () (driver·pci·rtl8139 dev iface)))
                     (start-networking iface)))
                  ((probe·pci·rtl8169? dev)
                   (log/info "Found an rtl8169 network card: " dev)
                   (let ((iface (make-iface 'ethernet)))
                     (spawn-fiber (lambda () (driver·pci·rtl8169 dev iface)))
                     (start-networking iface)))
                  ((probe·pci·eepro100? dev)
                   (log/info "Found an eepro100 network card: " dev)
                   (let ((iface (make-iface 'ethernet)))
                     (spawn-fiber (lambda () (driver·pci·eepro100 dev iface)))
                     (start-networking iface)))))
          devs)

(for-each (lambda (dev)
            (when (probe·pci·ehci? dev)
              (log/info "Found an EHCI controller: " dev)
              (spawn-fiber
               (lambda ()
                 (let ((controller (make-usb-controller)))
                   ;; (spawn-fiber (lambda () (manage-usb-hci controller)))
                   (driver·pci·ehci dev controller))))))
          devs)

(for-each (lambda (dev)
            (when (probe·pci·uhci? dev)
              (log/info "Found a UHCI controller: " dev)
              (spawn-fiber
               (lambda ()
                 (let ((controller (make-usb-controller)))
                   (spawn-fiber (lambda ()
                                  (parameterize ([current-log-fields
                                                  (append '(SUBSYSTEM "usb")
                                                          (current-log-fields))])
                                    (manage-usb-hci controller))))
                   (driver·pci·uhci dev controller))))))
          devs)

(screen-refresh)
