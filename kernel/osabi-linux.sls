;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2021 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; (Extremely limited) Linux ABI compatibility

;; This is a proof of concept and a way to test "Hello World"
;; programs. It has been tested with: statically linked assembler
;; programs, statically linked musl programs and Free Pascal programs.

;; It is intended for Loko on bare metal. For running Linux program on
;; the Linux kernel, see (pre-srfi processes).

(library (loko kernel osabi-linux)
  (export
    spawn-linux-process)
  (import
    (rnrs)
    (loko system $host)
    (loko system fibers)
    (loko system logging)
    (loko system unsafe)
    (loko match)
    (loko kernel binfmt-elf)
    (only (loko) port-buffer-mode-set!)

    (loko arch amd64 pc-ustate)
    (loko arch amd64 linux-numbers))

(define (log/x severity x*)
  (send-log severity
            (call-with-string-output-port
              (lambda (p)
                (for-each (lambda (x) (display x p)) x*)))))

(define (log/debug . x*) (log/x DEBUG x*))
(define (log/warning . x*) (log/x WARNING x*))
(define (log/critical . x*) (log/x CRITICAL x*))

(define (open-user-memory-input-port pt)
  (define vaddr 0)
  (define &paddr #f)
  (define psize 0)
  (define (read! bv start count)
    (cond ((not (<= 0 vaddr (- (expt 2 47) 1)))
           0)
          (else
           (when (eqv? psize 0)
             (let-values ([(&paddr^ psize^) (page-table-lookup pt vaddr #f)])
               (set! &paddr &paddr^)
               (set! psize (or psize^ 0))))
           (if (eqv? psize 0)
               0
               (do ((n (fxmin count psize))
                    (i 0 (fx+ i 1)))
                   ((fx=? i n)
                    (set! &paddr (fx+ &paddr n))
                    (set! psize (fx- psize 1))
                    (set! vaddr (fx+ vaddr n))
                    n)
                 (bytevector-u8-set! bv (fx+ start i)
                                     (get-mem-u8 (fx+ &paddr i))))))))
  (define (getpos)
    vaddr)
  (define (setpos pos)
    (set! vaddr pos)
    (set! psize 0))
  (let ((p (make-custom-binary-input-port "usermem" read! getpos setpos #f)))
    (port-buffer-mode-set! p (buffer-mode none))
    p))

(define (open-user-memory-output-port pt)
  (define vaddr 0)
  (define &paddr #f)
  (define psize 0)
  (define (write! bv start count)
    (cond ((not (<= 0 vaddr (- (expt 2 47) 1)))
           0)
          (else
           (when (eqv? psize 0)
             (let-values ([(&paddr^ psize^) (page-table-lookup pt vaddr #t)])
               (when (not &paddr^)
                 (error #f "User memory not accessible for writes" vaddr))
               (set! &paddr &paddr^)
               (set! psize (or psize^ 0))))
           (do ((n (fxmin count psize))
                (i 0 (fx+ i 1)))
               ((fx=? i n)
                (set! &paddr (fx+ &paddr n))
                (set! psize (fx- psize 1))
                (set! vaddr (fx+ vaddr n))
                n)
             (put-mem-u8 (fx+ &paddr i)
                         (bytevector-u8-ref bv (fx+ start i)))))))
  (define (getpos)
    vaddr)
  (define (setpos pos)
    (set! vaddr pos)
    (set! psize 0))
  (let ((p (make-custom-binary-output-port "usermem" write! getpos setpos #f)))
    (port-buffer-mode-set! p (buffer-mode none))
    p))

(define (linux-write pt ustate fdesc vaddr size)
  (let ((size (fxmin #x7ffff000 size)))
    (cond
      ((not fdesc)
       (- EBADF))
      ((fx<? size 0)
       (- EINVAL))
      ((eqv? size 0)
       0)
      (else
       (let ((umem (open-user-memory-input-port pt)))
         (set-port-position! umem vaddr)
         (match (get-message fdesc)
           [(resp-ch bv start count)
            (let ((n (get-bytevector-n! umem bv start (fxmin count size))))
              (cond ((eof-object? n)
                     (put-message resp-ch -1)
                     (- EFAULT))
                    (else
                     (put-message resp-ch n)
                     n)))]))))))

(define (linux-writev pt ustate fdesc *iov iovcnt)
  (let ((umem (open-user-memory-input-port pt)))
    (set-port-position! umem *iov)
    (cond
      ((not fdesc)
       (- EBADF))
      ((fx<? iovcnt 0)
       (- EINVAL))
      (else
       (let lp ((written 0) (iovcnt iovcnt))
         (cond
           ((eqv? iovcnt 0)
            written)
           (else
            (let ((bv (get-bytevector-n umem 16)))
              (if (or (eof-object? bv) (not (eqv? (bytevector-length bv) 16)))
                  (if (eqv? written 0)
                      (- EFAULT)
                      written)
                  (let ((iov_base (bytevector-u64-native-ref bv 0))
                        (iov_len (bytevector-u64-native-ref bv 8)))
                    (let ((n (linux-write pt ustate fdesc iov_base iov_len)))
                      (if (fx<? n 0)
                          (if (eqv? written 0)
                              n
                              written)
                          (lp (fx+ written n) (fx- iovcnt 1))))))))))))))

(define (linux-nanosleep pt ustate *req *rem)
  (let ((umem (open-user-memory-input-port pt)))
    (set-port-position! umem *req)
    (let ((bv (get-bytevector-n umem 16)))
      (if (or (eof-object? bv) (not (eqv? (bytevector-length bv) 16)))
          (- EFAULT)
          (let ((tv_sec (bytevector-u64-native-ref bv 0))
                (tv_nsec (bytevector-u64-native-ref bv 8)))
            (sleep (+ tv_sec (/ tv_nsec (expt 10 9))))
            (when (not (eqv? *rem 0))
              (let ((umem (open-user-memory-output-port pt)))
                (set-port-position! umem *rem)
                (bytevector-fill! bv 0)
                (put-bytevector umem bv)))
            0)))))

(define (linux-arch_prctl pt ustate code addr)
  (cond ((eqv? code ARCH_SET_FS)
         (cond ((fx<? -1 addr (expt 2 47))
                (let ((fsbase (get-mem-s61 (fx+ ustate USTATE:FSBASE))))
                  (put-mem-s61 (fx+ ustate USTATE:FSBASE) addr)
                  fsbase))
               (else (- EFAULT))))
        (else (- ENOSYS))))

(define (wrap-output-port output-port ch)
  (spawn-fiber (lambda ()
                 (define (read! bv start count)
                   (let ((resp-ch (make-channel)))
                     (put-message ch (list resp-ch bv start count))
                     (let ((n (get-message resp-ch)))
                       (if (eq? n 'fail)
                           (read! bv start count)
                           n))))
                 (let* ((pb (make-custom-binary-input-port "fd" read! #f #f #f))
                        (p (transcoded-port pb (native-transcoder))))
                   ;; FIXME: No quite efficient, is it, and it doesn't
                   ;; work as an implementation of pipes. But this is
                   ;; used to get transcoding of UTF-8 when printing
                   ;; to the console.
                   (let lp ()
                     (put-char output-port (get-char p))
                     (lp))))))

(define (handle-linux-process pid pt ustate fds)
  (define (loop)
    (match (perform-operation (wait-process-operation pid))
      ('syscall
       (let ((rax (get-mem-s61 (fx+ ustate USTATE:RAX)))
             (rdi (get-mem-s61 (fx+ ustate USTATE:RDI)))
             (rsi (get-mem-s61 (fx+ ustate USTATE:RSI)))
             (rdx (get-mem-s61 (fx+ ustate USTATE:RDX))))
         (define __NR_writev 20)
         (define __NR_nanosleep 35)
         (log/debug (list 'syscall rax
                          'rdi (number->string rdi 16)
                          'rsi (number->string rsi 16)
                          'rdx (number->string rdx 16)))

         (cond
           ((eqv? rax __NR_write)
            (put-mem-s61 (fx+ ustate USTATE:RAX)
                         (linux-write pt ustate (hashtable-ref fds rdi #f)
                                      rsi rdx))
            (process-resume pid)
            (loop))

           ((eqv? rax __NR_writev)
            (put-mem-s61 (fx+ ustate USTATE:RAX)
                         (linux-writev pt ustate (hashtable-ref fds rdi #f)
                                       rsi rdx))
            (process-resume pid)
            (loop))

           ((eqv? rax __NR_nanosleep)
            (put-mem-s61 (fx+ ustate USTATE:RAX)
                         (linux-nanosleep pt ustate rdi rsi))
            (process-resume pid)
            (loop))

           ((or (eqv? rax __NR_exit)
                (eqv? rax __NR_exit_group))
            rdi)

           ((eqv? rax __NR_arch_prctl)
            (put-mem-s61 (fx+ ustate USTATE:RAX)
                         (linux-arch_prctl pt ustate rdi rsi))
            (process-resume pid)
            (loop))

           (else
            (log/warning "Unimplemented Linux syscall " rax)
            (put-mem-s61 (fx+ ustate USTATE:RAX) (- ENOSYS))
            (process-resume pid)
            (loop)))))

      ('trap
       (log/critical "Traps are not implemented yet. "
                     (list 'rip (number->string (get-mem-s61 (fx+ ustate USTATE:RIP)) 16)
                           'fault (get-mem-s61 (fx+ ustate USTATE:FAULT-NUMBER))
                           'code (get-mem-s61 (fx+ ustate USTATE:FAULT-CODE))
                           'cr2 (number->string (get-mem-s61 (fx+ ustate USTATE:CR2)) 16)))
       #f)

      (x
       (log/critical "Unknown process message: " x)
       #f)))

  (let ((status (guard (exn
                        ((serious-condition? exn)
                         (send-log CRITICAL "Exception while handling process"
                                   'EXCEPTION exn)
                         #f))
                  (loop))))
    (log/debug "exit status: " status)
    (process-exit pid status)
    (page-table-free! pt)
    ($free-ustate ustate)))

(define (spawn-linux-process path command-line environment)
  (define fds (make-eqv-hashtable))
  (define auxv
    `((,AT_PAGESZ . 4096)
      (,AT_PLATFORM . ,(string->utf8 "x86_64"))
      (,AT_EXECFN . ,(string->utf8 path))
      (,AT_FLAGS . 0)
      (,AT_UID . 0)
      (,AT_EUID . 0)
      (,AT_GID . 0)
      (,AT_EGID . 0)))
  (let ((stdin (make-channel))
        (stdout (make-channel))
        (stderr (make-channel)))
    ;; TODO: Handle stdin
    (hashtable-set! fds 1 stdout)
    (hashtable-set! fds 2 stderr)
    (wrap-output-port (current-output-port) stdout)
    (wrap-output-port (current-error-port) stderr))
  (let-values ([(pt ustate) (binfmt-elf-load path command-line environment auxv)])
    (define pid (new-usermode-process ustate))
    (spawn-fiber
     (lambda ()
       (handle-linux-process pid pt ustate fds)))
    pid)))
