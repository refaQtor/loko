## Opening issues

No special guidelines out of the ordinary. Nobody will bite you.

## Submitting code

If you want to share something smallish that doesn't really belong
anywhere then you can add it to the `contrib/` directory. Before
working on something large that you hope to get merged, please reach
out and discuss it first.

You retain ownership of the code you contribute. The code you
contribute is assumed to be contributed under the same license as the
project is already using, as is customary for open source and free
software projects. Other AGPLv3+-compatible licenses are also okay.

When you contribute code, use `git commit -s` or add the
`Signed-off-by:` line yourself. This has the same meaning as in the
Linux project, and refers to
the
[Developer Certificate of Origin](https://developercertificate.org/).

## Where to contribute code

Use one of these options:

1. Open a merge request on GitLab (https://gitlab.com/weinholt/loko).
   This requires an account, so there are other options:

2. Use `git format-patch` and post it on comp.lang.scheme. If the
   traffic get too much we'll try to make another newsgroup.

3. Format a patch and use email (see below).

For new files, please write a copyright line and an
`SPDX-License-Identifier` at the top. You can check existing files to
see how it is done.

## Contact by email

If you don't want to open an account on GitLab or mess around
with Usenet then you can use email.

Send your bugs and patches to the `bugs` alias at the domain of the
Loko Scheme website. They will be forwarded to GitLab and will
initially be marked as confidential. You will get a confirmation when
the email has been received.
