# Version 0.7.0

This version fixes bugs, improves performance and adds features.

* A bootable hard drive image with the `bin/pc-repl` program is now
  available.

* New experimental TCP/IP stack. The `bin/pc-repl` program now has a
  network REPL that listens to port 23. The TCP/IP stack is also
  possible to use on Linux with `(loko drivers net tun)`.

* Updated SRFI 170 to match the finalized version.

* Fixed `flround` etc to not require SSE 4.1.

* Fixed transcoding when a codepoint straddles buffers.

* Added `pretty-print` to `(loko)`, based on code by Marc Feeley.

* Fixed `time-it*` for CPUs that have a lower TSC resolution.

* Fixed the condition reported when a program calls a non-procedure.

* Improved speed of `symbol->string`, `string->symbol` and various
  list procedures.

* Fixed R6RS `assert` syntax so it returns the true value.

* Fixed how R7RS `include` searches for files.

* Various fixes to the `(pre-srfi processes)` library.

* The RTC driver now has an external API.

* The `bin/pc-repl` program now attempts to set a graphics mode using
  VBE with the Video BIOS from the graphics card.

* Fixed a missed interrupt in the rtl8139 driver. The driver now also
  pads short frames before transmitting them.

See git for a full list of changes.

# Version 0.6.0

Loko Scheme 0.6.0 introduces support for R7RS-small. The release
tarballs now include a pre-built compiler and all dependencies needed
for building Loko.

This version also introduces new features, bug fixes and other
changes:

* Floating point numbers are now printed in decimal or scientific
  format (depending on the exponent).

* Improved tracking of source code location and reporting of arity
  assertions.

* Updated SRFI 170 to draft #10.

* Added SRFI 174 and SRFI 198 (draft #3).

* New `load-program` procedure that can be used to load and run
  top-level programs.

* The `--script` argument now uses script semantics (similar to
  `load`) instead of top-level semantics.

* The new `(loko apropos)` library lets you look up exported names in
  environments and libraries.

* The compiler no longer prints every file and library it is working
  with.

* Fixed an infinite loop in cp0's handling of `not`.

Please be aware that as part of introducing R7RS support, a small
inconsistency with R6RS has been introduced. Vectors are currently
self-evaluating even in R6RS code. This will be fixed in a later
version.

See git for a full list of changes.

# Version 0.5.0

With this version only a single test in the Racket R6RS test suite
fails. There should be no incompatibilities with previous versions.
Here are some of the improvements:

* The `module` syntax is exported in the `(loko)` module, allowing the
  definition of internal modules. This is provided for compatibility
  with existing code.

* The `(rnrs unicode)` library is now completely implemented thanks to
  the use of the R6RS unicode implementation by Abdulaziz Ghuloum and
  R. Kent Dybvig.

* The `(rnrs io ports)` library has received a significant speed
  boost.

* Improved support for multiple return values. Up to six return values
  can be passed directly in registers.

* Loko now uses [laesare](https://akkuscm.org/packages/laesare/) as
  its reader. This means that line and column information is available
  on assertions and procedures. This reader also supports the R7RS
  lexical syntax.

* Loko now checks variable references in `letrec` and `letrec*` to
  ensure that variables are not used before they are defined.

* The .bss section is now generated correctly, so Linux perf is now
  willing to use the symbol table.

* The `bin/pc-repl` program is a new graphical REPL that currently
  runs on virtual machines.

* `map` is now tail recursive and does not build up a large stack,
  which fixes a pathological case in some programs that use `call/cc`
  from inside map.

See git for a full list of changes.
