;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;; R7RS-small standard library

(library (scheme lazy)
  (export
    delay
    delay-force
    force
    promise?
    make-promise)
  (import
    (rnrs)
    (rnrs mutable-pairs))

;; This implementation is from R7RS-small

(define-syntax delay-force
  (syntax-rules ()
    ((delay-force expression)
     (make-promise* #f (lambda () expression)))))

(define-syntax delay
  (syntax-rules ()
    ((delay expression)
     (delay-force (make-promise* #t expression)))))

(define (make-promise obj)
  (make-promise* #t obj))

(define make-promise*
  (lambda (done? proc)
    (list (cons done? proc))))

(define (force promise)
  (if (promise-done? promise)
      (promise-value promise)
      (let ((promise* ((promise-value promise))))
        (unless (promise-done? promise)
          (promise-update! promise* promise))
        (force promise))))

(define promise-done?
  (lambda (x) (car (car x))))

(define promise-value
  (lambda (x) (cdr (car x))))

(define promise-update!
  (lambda (new old)
    (set-car! (car old) (promise-done? new))
    (set-cdr! (car old) (promise-value new))
    (set-car! new (car old))))

(define (promise? obj)
  (and (pair? obj)
       (null? (cdr obj))
       (pair? (car obj))
       (boolean? (caar obj)))))
